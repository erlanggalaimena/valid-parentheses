package main

import (
	"fmt"
)

func main() {
	fmt.Println(isValid("()"))
	fmt.Println(isValid("()[]{}"))
	fmt.Println(isValid("(]"))
	fmt.Println(isValid("){}"))
}

func isValid(s string) bool {
	result := true

	if len(s)%2 != 0 {
		result = false
	} else {
		parenthesesList := make([]rune, len(s))

		index := 0

		for _, char := range s {
			if index == 0 && (char == ')' || char == ']' || char == '}') {
				result = false

				break
			} else if index == 0 {
				parenthesesList[index] = char

				index++
			} else {
				if parenthesesList[index-1] == '(' && char == ')' {
					index--
				} else if parenthesesList[index-1] == '[' && char == ']' {
					index--
				} else if parenthesesList[index-1] == '{' && char == '}' {
					index--
				} else if char == ')' || char == ']' || char == '}' {
					result = false

					break
				} else {
					parenthesesList[index] = char

					index++
				}
			}
		}

		if result {
			result = index == 0
		}
	}

	return result
}
